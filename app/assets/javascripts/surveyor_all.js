//= require surveyor/jquery-ui-timepicker-addon
//= require surveyor/jquery.selectToUISlider
//= require surveyor/jquery.surveyor
//= require surveyor/jquery.maskedinput
//= require smart/jquery.validate
//= require surveyor/jquery.validate.additional-methods
//= require surveyor/surveyor_validations

(function( $ ) {
	jQuery('input.age[type="number"]').attr({
	  "max": 100,
	  "min": 1
	});

	jQuery('input.living_in_house[type="number"]').attr({
	  "max": 100,
	  "min": 0
	});

	jQuery("input[type='number']").attr("type", "text");
})( jQuery );
