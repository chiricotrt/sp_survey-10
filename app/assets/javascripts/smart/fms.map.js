var smart;
if (smart === undefined) {
	smart = {};
}

smart.Map = {};
smart.Map._frequentPlaces = {};
smart.Map._tempMarker = null;

smart.Map.clearMap = function(){
	if (smart.Map._markers != undefined){
		for(var i=0; i <smart.Map._markers.length;i++){
			smart.Map._markers[i].setMap(null);
		}
	}

	if (smart.Map._markers != undefined){
		for(var i=0; i <smart.Map._traces.length;i++){
			smart.Map._traces[i].setMap(null);
		}
	}

	smart.Map._markers = [];  // just the stops
	smart.Map._traces = [];  // the polyline constructed from pts from trace segments and stops
}

smart.Map.init = function(){
	smart.Map._latlng = new google.maps.LatLng(1.359516,103.844748);
	smart.Map._latlngReseted = false;

	var myOptions = {
				zoom: 10,
				center: smart.Map._latlng,
				streetViewControl: false,
				zoomControl: true,
				scaleControl: true,
				scaleControlOptions: {
					position: google.maps.ControlPosition.BOTTOM_LEFT
				}
	};

	smart.Map._map = new google.maps.Map(document.getElementById("mapa"), myOptions);

 	smart.Map._arrow = {
    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
  };

	if(navigator.geolocation) {
		browserSupportFlag = true;
		navigator.geolocation.getCurrentPosition(function(position) {
			if (!smart.Map._latlngReseted){
		  	smart.Map._latlng = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
		  	//console.log("SETTING  CENTER WITH Geolocation", smart.Map._latlng);
				smart.Map._map.setCenter(smart.Map._latlng);
				smart.Map._latlngReseted = true;
			}
		},
		function() {
		  smart.Map.handleNoGeolocation(browserSupportFlag);
		});
	}
	else if (google.gears) {
		// Try Google Gears Geolocation
		browserSupportFlag = true;
		var geo = google.gears.factory.create('beta.geolocation');
		geo.getCurrentPosition(function(position) {
			if (!smart.Map._latlngReseted){
				smart.Map._latlng = new google.maps.LatLng(position.latitude,position.longitude);
				//console.log("SETTING  CENTER WITH gears", smart.Map._latlng);
				smart.Map._map.setCenter(smart.Map._latlng);
				smart.Map._latlngReseted = true;
			}
		},
		function() {
			smart.Map.handleNoGeolocation(browserSupportFlag);
		});
	} else {
		  // Browser doesn't support Geolocation
		  browserSupportFlag = false;
			smart.Map.handleNoGeolocation(browserSupportFlag);
	}

	smart.Map.loadAutocomplete();

	smart.Map._markers = [];
	smart.Map._traces = [];
	smart.Map._path = [];
	smart.Map._tempMarker = null;

	jQuery.unsubscribe("activity-deleted", smart.Map.deleteActivity);
	jQuery.subscribe("activity-deleted", smart.Map.deleteActivity);

	jQuery.unsubscribe("activities-fetched", smart.Map.renderMap);
	jQuery.subscribe("activities-fetched", smart.Map.renderMap);

	jQuery.unsubscribe("update-main-activity", smart.Map.updateActivityMarker);
	jQuery.subscribe("update-main-activity", smart.Map.updateActivityMarker);

	jQuery.unsubscribe("center-map", smart.Map.centerMapInMarker);
	jQuery.subscribe("center-map", smart.Map.centerMapInMarker);

	jQuery.unsubscribe("redraw", smart.Diary.renderMap);
	jQuery.subscribe("redraw", smart.Diary.renderMap);

}

smart.Map.handleNoGeolocation = function(errorFlag){

	/*
	if (errorFlag) {
		smart.Map._latlng = new google.maps.LatLng(1.359516,103.844748);
	} else {
		smart.Map._latlng = new google.maps.LatLng(1.359516,103.844748);
	}*/
	//console.log("SETTING  CENTER WITHOUT LOCATION", smart.Map._latlng);
	smart.Map._map.setCenter(smart.Map._latlng);
}

smart.Map.addMarkerToMap = function(map, pos, label, idx, activity_idx, image){
	var marker = new MarkerWithLabel({
						position: pos,
						map: map,
						draggable: false,
						raiseOnDrag: true,
						icon: '/assets/smart/markers/' + image + '.png',
						shadow: '/assets/shadow.png',
						name: label
  });
  map.setCenter(pos);

  infowindow = new google.maps.InfoWindow({
    content: 'default'
  });

	google.maps.event.addListener(marker, "click", function (mEvent) {
		map.setCenter(mEvent.latLng);
		infowindow.setContent(marker.name);
		infowindow.open(map, this);
	});

	google.maps.event.addListener(marker, "rightclick", function (mEvent) {
		smart.Map.removeFrequentPlace(idx, activity_idx);
	});

	google.maps.event.addListener(marker,"dragstart",function(event){
    lastPos = this.getPosition();
	});


	google.maps.event.addListener(marker,'dragend',function(event) {
		var pos = event.latLng;
		jQuery.post('/frequent_places/update',
			{
				id: idx,
				frequent_place: {
					lat: pos.lat(),
					lon: pos.lng()
				}
			},
			function(result){
				var fp = jQuery.parseJSON(result);
				if (!fp.hasOwnProperty("frequent_place")) marker.setPosition(lastPos);
		});
  });

  google.maps.event.addListenerOnce( marker, "visible_changed", function() {
    infowindow.close();
  });

  return marker;
}

smart.Map.addActivityMarkerToMap = function(image_name, stop_id, stop_source_id, stop_lat, stop_lon, validated, counter, marker_idx){
	var icon = '/assets/smart/markers/'+image_name+'.png';
	if ((validated & 0x1) > 0){
		icon = '/assets/smart/markers/'+image_name+'-checked.png';
	};
	var marker_pos = new google.maps.LatLng(stop_lat,stop_lon);
	var marker = new MarkerWithLabel({
						position: marker_pos,
						map: smart.Map._map,
						draggable: true,
						raiseOnDrag: true,
						icon: icon,
						shadow: '/assets/shadow.png',
						labelContent: ''+counter,
						labelAnchor: new google.maps.Point(-5,43),
						labelClass: 'markerlabels',
						labelStyle: {opacity: 1}
    });
    marker.stopid = stop_id;
    marker.source_id = stop_source_id;
    marker.previousPosition = marker_pos;
    smart.Map._markers.push(marker);

		// var p = smart.Map._traces[0].getPath();
		// p.push(marker_pos);
		// var len = p.getLength();
  	// marker.bindTo('position', smart.Map._traces[0].binder, marker_idx.toString());
    // console.log('len:', len, marker_idx);

	google.maps.event.addListener(marker, "dragend", function (mEvent) {

 		var curr_row = counter-1;
		var dataToUpdate = {};
		dataToUpdate["stop_lat"]= mEvent.latLng.lat();
		dataToUpdate["stop_lng"]= mEvent.latLng.lng();
		dataToUpdate["previous_stop_lat"]= marker.previousPosition.lat();
		dataToUpdate["previous_stop_lng"]= marker.previousPosition.lng();

		previousStop = null;
		nextStop = null;

		currentStop = smart.utils.buildValidatorObject(smart.Diary._currentDay['stops'][curr_row].stop, dataToUpdate);
		//get previous stop
		if (curr_row-1 >= 0){
			previousStop = smart.utils.buildValidatorObject(smart.Diary._currentDay['stops'][curr_row-1].stop, {});
		}

		//get next stop
		if (curr_row+1 < smart.Diary._currentDay.length){
			nextStop = smart.utils.buildValidatorObject(smart.Diary._currentDay['stops'][curr_row+1].stop, {});
		}

 		var validatorResult = smart.ad_validator.run(['activity_location'], currentStop, previousStop, nextStop);
 		console.log(previousStop, currentStop, validatorResult);

		var moveMoreThanAlert = function(step){
			jQuery('#confirm_marker_move').fm('dialog.confirm', function(elm, res){
				if (res === "yes"){
					jQuery.publish(step._stepId,[{move_marker: true}]);
				} else {
					jQuery.publish(step._stepId,[{move_marker: false}]);
				}
			});
		}

		var speedingAlert = function(step){
			if ("move_marker" in step._output && step._output.move_marker == false){
				jQuery.publish(step._stepId,[step._output]);
				return;
			}

			jQuery('#move_with_impossible_speed').fm('dialog.confirm', function(elm, res){
				if (res === "yes"){
					jQuery.publish(step._stepId,[{move_marker: true}]);
				} else {
					jQuery.publish(step._stepId,[{move_marker: false}]);
				}
			});
		}

		var moveMarkerAfterHook = function(data){
			console.log('move marker', data);
			if ("move_marker" in data && data.move_marker){
				jQuery.post('/report/saveAfterDrag', {index: stop_id, coords: (mEvent.latLng.lat() + ',' + mEvent.latLng.lng()) })
					.done(function(answer) {
						jQuery("#stop_" + answer.stop.id + "_address").text(answer.stop.address); // Redraw address if changed
					});
				jQuery.publish('marker:dragend', [ stop_id , mEvent.latLng.lat(), mEvent.latLng.lng() ]);
				marker.previousPosition = mEvent.latLng;
				console.log(mEvent.latLng.lat() + ',' + mEvent.latLng.lng());
			} else {
				// marker.unbind('position');
				// var path = smart.Map._traces[0].getPath();
				// path.setAt(marker_idx,marker.previousPosition);
				marker.setPosition(marker.previousPosition);
				// marker.bindTo('position', smart.Map._traces[0].binder, marker_idx.toString());
			}
		}

		var moveMarkerSequence = new smart.Sequencer();
		moveMarkerSequence.registerAfterHook(moveMarkerAfterHook);

		if(validatorResult.length != 0){
			if (_.find(validatorResult, function(elm){ return elm.errorCode === 1040; })) { // Dragged more than 500m
				moveMarkerSequence.registerStep(new smart.SequenceStep(moveMoreThanAlert, function(){} ));
			}

			if (_.find(validatorResult, function(elm){ return elm.errorCode === 1011; })) { // New Position implies impossible speed
				moveMarkerSequence.registerStep(new smart.SequenceStep(speedingAlert, function(){} ));
			}

			if (_.find(validatorResult, function(elm){ return elm.errorCode === 1010; })) { // New Position implies impossible speed
				moveMarkerSequence.registerStep(new smart.SequenceStep(speedingAlert, function(){} ));
			}
		}

		if (validatorResult.length === 0) {
			if (typeof(ga) == "function") {
				ga('send', 'event', 'button', 'drag', 'moved stop');
			}
			jQuery.post('/report/saveAfterDrag',{ index: stop_id, coords: (mEvent.latLng.lat() + ',' + mEvent.latLng.lng())})
				.done(function(answer) {
					jQuery("#stop_" + answer.stop.id + "_address").text(answer.stop.address); // Redraw address if changed
				});

			jQuery.publish('marker:dragend', [ stop_id , mEvent.latLng.lat(), mEvent.latLng.lng() ]);
			marker.previousPosition = mEvent.latLng;
		} else {
			moveMarkerSequence.run();
		}
		//updateTutorialStep('drag_marker');
	});

	return marker;
}

smart.Map.addSingleTraceToMap = function(stops){
	var vertexCount = 0;
	var lastAccuracy = -1;

	var line = new google.maps.Polyline({
						strokeColor: '#881117',
						strokeOpacity: 0.8,
						strokeWeight: 8,
						map: smart.Map_map
	});
	line.binder = new MVCArrayBinder(line.getPath());

  smart.Map._traces.push(line);


	jQuery.each(smart.Diary._currentDay['stops'], function(index, value){
		// user's trace take precedence until we figure out a way to merge user stop into the generated trace
		if (index > 0 && smart.Diary._currentDay['stops'][index-1].stop.source_id != 2) {
			// every stop has a list of value.stop_trace_segment
			var traceSegments = value.stop.stop_trace_segment;
			for (var k=0; k < traceSegments.length - 1; k++) {
				// each stop_trace_segment has at least two points, we are joining them into a single polyline
				var segment = traceSegments[k];
				var path= google.maps.geometry.encoding.decodePath(segment.encoded_points);

				for (var m=1; m < path.length; m++) {
					var pos = path[m];

					console.log('path append', k, index, pos);
					var p = line.getPath();
					p.push(pos);
					var len = p.getLength();

					var canDrag = false;
					// for future: if the next trace is not accurate, allow the user to drag
					// if (segment.trace_accuracy_level_id == 2) {
					// 	if (k > 0 && (index < traceSegments.length -1) && traceSegments[index+1].trace_accuracy_level_id == 2) canDrag = true;
					// }

					var marker = new MarkerWithLabel({
						position: pos,
						map: smart.Map._map,
						draggable: canDrag,
						raiseOnDrag: true,
						icon: ' ',
						// shadow: '/assets/shadow.png',
						// labelContent: '#' + vertexCount,
						// labelAnchor: new google.maps.Point(-5,43),
						labelClass: 'markerlabels',
						labelStyle: {opacity: 1}
			    });


			    // var marker = new google.maps.Marker({
			    //   position: pos,
			    //   title: '#' + vertexCount + ':' + segment.trace_accuracy_level_id,
			    //   map: smart.Map._map,
			    //   draggable : canDrag
			    // });

			    marker.previousPosition = pos;
			    marker.bindTo('position', line.binder, vertexCount.toString());
			    // console.log('len:', len, vertexCount);

					vertexCount++;
				}
			}
		}
		console.log('activity marker', value.stop);
		smart.Map.addActivityMarkerToMap(value.stop.main_activity, value.stop.id, value.stop.source_id, parseFloat(value.stop.lat), parseFloat(value.stop.lon), value.stop.validated, index+1, vertexCount);
		vertexCount++;
	});

	line.setMap(smart.Map._map);

	// console.log("attaching listeners to polyline");
	google.maps.event.addListener(line, 'mouseover', function (event) {
    this.setOptions({
      strokeOpacity: 1,
			icons: [{
				icon: smart.Map._arrow,
				offset: '100%'
			}]
    });
  });

  google.maps.event.addListener(line, 'mouseout', function (event) {
    this.setOptions({
       strokeOpacity: 0.8,
       icons: null
    });
  });
}

smart.Map.addTraceToMap = function(traceSegments){
	for (var i=0; i < traceSegments.length; i++){
		// unlike js.erb in backoffice, the string is only escaped once so double-backslashes need to be replaced
		var path_string = traceSegments[i].encoded_points.replace(/\\\\/g, '\\');
		var path = google.maps.geometry.encoding.decodePath(path_string);
		var line = new google.maps.Polyline({
							strokeColor: '#881117',
							strokeOpacity: 0.3,
							strokeWeight: 8,
							// clickable: false,
							zIndex: 1,
							path: path
							// icons: [{
							// 	icon: smart.Map._arrow,
							// 	offset: '100%'
							// }]
		});

		line.setMap(smart.Map._map);

		// console.log("attaching listeners to polyline");
		google.maps.event.addListener(line, 'mouseover', function (event) {
      this.setOptions({
        strokeOpacity: 0.5
      });
	  });

    google.maps.event.addListener(line, 'mouseout', function (event) {
      this.setOptions({
         strokeOpacity: 0.3,
         icons: null
      });
    });

		smart.Map._traces.push(line);
	}
}

smart.Map.renderMap = function(e)
{
	smart.Map.clearMap();
	jQuery.each(smart.Diary._currentDay['stops'], function(index, value){
		smart.Map.addActivityMarkerToMap(value.stop.main_activity, value.stop.id, value.stop.source_id, parseFloat(value.stop.lat), parseFloat(value.stop.lon), value.stop.validated, index+1, 0);
		if (index > 0) {
			var traceArray = value.stop.stop_trace_segment;
			smart.Map.addTraceToMap(traceArray);
		}
	});

	smart.Map.centerMapOverview();
}

smart.Map.deleteActivity = function(e, act_id, index)
{
	var marker = smart.Map._markers.splice(index,1);
	marker[0].setMap(null);
	smart.Map.updateMarkerIndexes();
	return true;
}

smart.Map.updateMarkerIndexes = function(){
	jQuery.each(smart.Map._markers, function(index, item){
		item.labelContent = ""+(index+1);
		item.label.draw();
	});
}

smart.Map.updateActivityMarker = function(ev, params){
	var validated = (params["status"] & 0x1) > 0;
	var icon = "/assets/smart/markers/"+params["main_activity"]+".png"
	if (validated) {
		icon = "/assets/smart/markers/"+params["main_activity"]+"-checked.png"
	};
	smart.Map._markers[params["row"]].setIcon(icon);
}

smart.Map.centerMapInMarker = function(ev, params){
	var pos = params["row"];

	if (params["type"] === "activity") {
		smart.Map.centerMapAroundMarker(pos);
	}
	else if (params["type"] === "travel") {
		smart.Map.centerMapBetweenMarkers(pos);
	}
}

smart.Map.getMarkerForStop = function(stop_id){
	var stop_contents = smart.Diary.getStopRowFromStopId(stop_id);
	// console.log(stop_contents["row"]);
	return smart.Map._markers[stop_contents["row"]];
}

smart.Map.centerMapOverview = function()
{
	if (smart.Map._markers.length == 0) return;

	var tempBounds = new google.maps.LatLngBounds();

	for(var i=0; i <= smart.Map._markers.length; i++){
		if (smart.Map._markers[i] != null) {
			tempBounds.extend(smart.Map._markers[i].getPosition());
		}
	}

	smart.Map._map.fitBounds(tempBounds);
	smart.Map._latlng = tempBounds.getCenter();
	smart.Map._latlngReseted = true;
}

smart.Map.centerMapAroundMarker = function(pos)
{
	smart.Map.setMarkersZIndex([pos]);

	var tempBounds = new google.maps.LatLngBounds();
	temp_pos = pos;

	for(var i=temp_pos-1; i <= temp_pos+1; i++){
		if (smart.Map._markers != null && smart.Map._markers[i] != null) {
			tempBounds.extend(smart.Map._markers[i].getPosition());
		}
	}

	var newZoom = smart.Map.getZoomByBounds(smart.Map._map, tempBounds);
	// console.log("new zoom = ",newZoom);
	if (newZoom < 14) {
		smart.Map._map.setCenter(smart.Map._markers[pos].getPosition(), 14);
	} else {
		smart.Map._map.fitBounds(tempBounds);
	}

}

smart.Map.centerMapBetweenMarkers = function(pos){

	smart.Map.setMarkersZIndex([pos, pos-1], 999, 0);

	var tempBounds = new google.maps.LatLngBounds();
	if (pos - 1 < 0)
		temp_pos = 1;
	else
		temp_pos = pos;

	for(var i=temp_pos-1; i <= temp_pos; i++){
		if (smart.Map._markers != null && smart.Map._markers[i] != null) {
			tempBounds.extend(smart.Map._markers[i].getPosition());
		}
	}
	smart.Map._map.fitBounds(tempBounds);
}


smart.Map.setMarkersZIndex = function(markerList, inListIndex, notInListIndex){
	if (inListIndex == undefined) { inListIndex = 999; }
	if (notInListIndex == undefined) { notInListIndex = 0; }

	for(var i=0;i<smart.Map._markers.length;i++){
		if (smart.Map._markers[i] != undefined){
			if (markerList.indexOf(i) == -1){
				smart.Map._markers[i].setZIndex(notInListIndex);
			} else {
				smart.Map._markers[i].setZIndex(inListIndex);
			}
		}
	}
}

/**
* Returns the zoom level at which the given rectangular region fits in the map view.
* The zoom level is computed for the currently selected map type.
* @param {google.maps.Map} map
* @param {google.maps.LatLngBounds} bounds
* @return {Number} zoom level
**/
 smart.Map.getZoomByBounds = function( map, bounds ){
  var MAX_ZOOM = map.mapTypes.get( map.getMapTypeId() ).maxZoom || 21 ;
  var MIN_ZOOM = map.mapTypes.get( map.getMapTypeId() ).minZoom || 0 ;

  var ne= map.getProjection().fromLatLngToPoint( bounds.getNorthEast() );
  var sw= map.getProjection().fromLatLngToPoint( bounds.getSouthWest() );

  var worldCoordWidth = Math.abs(ne.x-sw.x);
  var worldCoordHeight = Math.abs(ne.y-sw.y);

  //Fit padding in pixels
  var FIT_PAD = 40;

  for( var zoom = MAX_ZOOM; zoom >= MIN_ZOOM; --zoom ){
      if( worldCoordWidth*(1<<zoom)+2*FIT_PAD < jQuery(map.getDiv()).width() &&
          worldCoordHeight*(1<<zoom)+2*FIT_PAD < jQuery(map.getDiv()).height() )
          return zoom;
  }
  return 0;
}

// function to snap a marker to series of polylines: http://stackoverflow.com/a/10712410/663028
smart.Map.find_closest_point_on_path = function(drop_pt,path_pts){
  distances = new Array();//Stores the distances of each pt on the path from the marker point
  distance_keys = new Array();//Stores the key of point on the path that corresponds to a distance

  //For each point on the path
  $.each(path_pts,function(key, path_pt){
      //Find the distance in a linear crows-flight line between the marker point and the current path point
      var R = 6371; // km
      var dLat = (path_pt.lat()-drop_pt.lat()).toRad();
      var dLon = (path_pt.lng()-drop_pt.lng()).toRad();
      var lat1 = drop_pt.lat().toRad();
      var lat2 = path_pt.lat().toRad();

      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
              Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = R * c;
      //Store the distances and the key of the pt that matches that distance
      distances[key] = d;
      distance_keys[d] = key;

  });
  //Return the latLng obj of the second closest point to the markers drag origin. If this point doesn't exist snap it to the actual closest point as this should always exist
  return (typeof path_pts[distance_keys[_.min(distances)]+1] === 'undefined')?path_pts[distance_keys[_.min(distances)]]:path_pts[distance_keys[_.min(distances)]+1];
}

smart.Map.loadAutocomplete = function(){
	var input = document.getElementById('mapSearch');
	smart.Map._tempLocationInput = input;
	var autocomplete = new google.maps.places.Autocomplete(input);
	smart.Map._autocomplete = autocomplete;
	smart.Map._geocoder = new google.maps.Geocoder();

  var map = smart.Map._map;
	autocomplete.bindTo('bounds', map);

	// no need to set initial location since we are using the existing map

	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		// input.className = '';
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			// Inform the user that the place was not found and return.
			// input.className = 'notfound';
			return;
		}

		// If the place has a geometry, then present it on a map.
		if (place.geometry.viewport) {
			console.log(place.geometry.viewport);
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(17);  // Why 17? Because it looks good.
		}


		var geocodingCache = {};
		window.geocodingCache = geocodingCache;

		// var markerstopid = window.openStop;
		geocodingCache['newstop'] = {
			label: place.formatted_address,
			value: place.formatted_address,
			bounds: place.geometry.bounds,
			position: place.geometry.location
		};

		//console.log (markerstopid)
		var item = geocodingCache['newstop'];
		smart.Diary.addMarkerToModalMap(item.position, map);
	});
}

smart.Map.setTempMarker = function (map, pos, label){
	if (smart.Map._tempMarker) smart.Map._tempMarker.setMap(null);
	var marker = new MarkerWithLabel({
						position: pos,
						map: map,
						draggable: true,
						raiseOnDrag: true,
						icon: '/assets/smart/markers/activity-default-checked.png',
						shadow: '/assets/shadow.png',
						name: label
  });

	google.maps.event.addListener(marker, 'dragend', function () {
    smart.Map._geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
	      if (status == google.maps.GeocoderStatus.OK) {
        var input = document.getElementById('add_frequent_place_search');
        input.value = results[0].formatted_address;
      }
    });
  });
	map.setCenter(pos);
  smart.Map._tempMarker = marker;
}

smart.Map.loadFrequentPlacesMap = function(){
	var center = new google.maps.LatLng(1.359516,103.844748);
	var myOptions = {
		zoom: 14,
		center: center,
		streetViewControl: false,
		zoomControl: true,
		scaleControl: true,
		scaleControlOptions: {
			position: google.maps.ControlPosition.BOTTOM_LEFT
		}
	};
	var map = new google.maps.Map(document.getElementById("frequent_places_map"), myOptions);
	smart.Map._frequentPlacesMap = map;
	var input = document.getElementById('add_frequent_place_search');
	smart.Map._tempLocationInput = input;
	smart.Map._geocoder = new google.maps.Geocoder();
	var autocomplete = new google.maps.places.Autocomplete(input);
	// autocomplete.bindTo('bounds', map);

	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		// input.className = '';
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			// Inform the user that the place was not found and return.
			// input.className = 'notfound';
			return;
		}

		// If the place has a geometry, then present it on a map.
		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(17);  // Why 17? Because it looks good.
		}

		// var geocoder = new google.maps.Geocoder();
		// var geocodingCache = {};
		// window.geocodingCache = geocodingCache;

		// // var markerstopid = window.openStop;
		// geocodingCache['newstop'] = {
		// 	label: place.formatted_address,
		// 	value: place.formatted_address,
		// 	bounds: place.geometry.bounds,
		// 	position: place.geometry.location
		// };

		//console.log (markerstopid)
		// var item = geocodingCache['newstop'];
		var pos = place.geometry.location;
		var label = place.formatted_address;
	  //input.value = '';
		smart.Map.setTempMarker(map, pos, label);

	});

}

smart.Map.addFrequentPlace = function(){
	var place_category = jQuery('#frequent_place_user_activity_id   option:selected')
	var place_category_id = place_category.val();

	var pos = smart.Map._tempMarker.getPosition();
	var label = smart.Map._tempLocationInput.value;


	jQuery.post('/frequent_places',
		{	frequent_place: {
				lat: pos.lat(),
				lon: pos.lng(),
				description: label
			},
			user_activity_id: place_category_id
		},
		"script"
	);
}

smart.Map.removeFrequentPlace = function(idx, activity_idx){
	var prompt = confirm("Click OK to delete this marker.");
	if (prompt === true) {
		jQuery.post('/frequent_places/' + idx,
			{
				"_method":"delete",
				user_activity_id: activity_idx
			},
			function(result){
				var status = jQuery.parseJSON(result)["status"];
				if (status === "ok") {
					smart.Map.removeFrequentPlaceFromMap(idx, activity_idx);
				}
		});
	}
}

smart.Map.removeFrequentPlaceFromMap = function(idx, activity_idx){
	var row = jQuery(".frequent_place_row[data-id=" + idx + "][data-activity-id=" + activity_idx + "]");
	var row_id = idx + '-' + activity_idx;
	row.remove();
  // update the map
	smart.Map._frequentPlaces[row_id].setMap(null);
	delete smart.Map._frequentPlaces[row_id];
}

smart.Map.addFrequentPlaceToMap = function(map, pos, label, idx, place_category_id, image){
	var marker = smart.Map.addMarkerToMap(map, pos, label, idx, place_category_id, image);
	smart.Map._frequentPlaces[idx+'-'+place_category_id] = marker;
	smart.Map.resetTempMarker();
}

smart.Map.resetTempMarker = function() {
	if(smart.Map._tempMarker) smart.Map._tempMarker.setMap(null);
	smart.Map._tempMarker = null;
	var input = document.getElementsByName('address_input')[0];
	if (input !== undefined) input.value = '';
}

smart.Map.getDistance = function(p1, p2) {
  var R = 6378137; // Earth’s mean radius in meter
  var dLat = p2.lat().toRad() - p1.lat().toRad();
  var dLong = p2.lng().toRad() - p1.lng().toRad();
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(p1.lat().toRad) * Math.cos(p2.lat().toRad()) *
    Math.sin(dLong / 2) * Math.sin(dLong / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d; // returns the distance in meter
};


/** Converts numeric degrees to radians */
if (typeof(Number.prototype.toRad) === "undefined") {
  Number.prototype.toRad = function() {
    return this * Math.PI / 180;
  }
}


/*
 * Use bindTo to allow dynamic drag of markers to refresh poly.
 */

function MVCArrayBinder(mvcArray){
  this.array_ = mvcArray;
}
MVCArrayBinder.prototype = new google.maps.MVCObject();
MVCArrayBinder.prototype.get = function(key) {
  if (!isNaN(parseInt(key))){
    return this.array_.getAt(parseInt(key));
  } else {
    this.array_.get(key);
  }
}
MVCArrayBinder.prototype.set = function(key, val) {
  if (!isNaN(parseInt(key))){
    this.array_.setAt(parseInt(key), val);
  } else {
    this.array_.set(key, val);
  }
}

MVCArrayBinder.prototype.extend = function(other_array) {
	other_array.forEach(function(v) {this.array_.push(v)}, this.array_);
}
