class UserSessionsController < ApplicationController
  # impressionist

  def create
    # FIX: required for authlogic gem
    creds = params.require(:user_session).permit(:username, :password)
    @user_session = UserSession.new(creds)
    Rails.logger.debug "LOGIN DEBUG: Inside create ..."

    if @user_session.save
      Rails.logger.debug "LOGIN DEBUG: Session saved"
      session[:time_zone] = @user_session.record.time_zone

      # # if user has not completed pre-survey, go to overview page
      # if not @user_session.record.completedPresurvey
      #   Rails.logger.debug "LOGIN DEBUG: Incomplete pre-survey"
      #   # redirect_to :controller => "pages", :action => :accept_consent
      #   redirect_to pre_questionnaire_url
      # else
      #   Rails.logger.debug "LOGIN DEBUG: Completed pre-survey"
      # end
      redirect_to new_sp_experiment_path
    else
      Rails.logger.debug "LOGIN DEBUG: Session not saved"
      # check if user is activated
      if ( @user_session.attempted_record &&
          !@user_session.invalid_password? &&
          !@user_session.attempted_record.active?)
        Rails.logger.debug "LOGIN DEBUG: Need to activate ..."

        session[:attempt_email] = @user_session.username
        redirect_to activations_new_path
      else
        Rails.logger.debug "LOGIN DEBUG: Wrong username/password"
        flash[:notice] = "Incorrect email/username or password"
        flash[:class] = "content_error"
        # redirect_to pages_root_path
        redirect_to new_wsp_users_path
      end
    end
  end

  def change_timezone
    if (not params["user"]["time_zone"].nil?)
      tz = ActiveSupport::TimeZone.new(params["user"]["time_zone"])
      if (not tz.nil?)
        session[:time_zone] = params["user"]["time_zone"]
        current_user.update_attribute(:time_zone, params["user"]["time_zone"])
      end
    end

    flash[:notice] = "Updated your timezone to #{current_user.time_zone}."
    flash[:class] = "content_feedback"

    if (not params["goto"].nil?)
      redirect_to params["goto"]
    else
      redirect_to root_url
    end
  end

  def destroy
    begin
      @user_session = current_user_session
      @user_session.destroy
      flash[:reset_pass] = "Successfully logged out."
      flash[:class] = "content_feedback"
      # redirect_to pages_newuser_path
      redirect_to root_path
    rescue
      redirect_to root_path
    end
  end
end
