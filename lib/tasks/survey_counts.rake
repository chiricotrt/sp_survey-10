desc 'Show survey counts'
task survey_counts: :environment do
  start_date = Date.new(2021,6,22)

  query = SpExperiment.where("created_at >= ?", start_date)
  total_responses = query.count
  completed = 0
  counts = {intro: 0, bystander: [0] * 6, recipient: [0] * 7}
  query.each do |experiment|
    next if experiment.responses.nil?

    responses = experiment.responses
    counts[:intro] += 1 if responses['introduction']

    bc = responses['bystander_choice']
    if bc
      (1..6).each do |day|
        counts[:bystander][day-1] += 1 if bc["day:#{day}"]
      end
    end

    rc = responses['recipient_choice']
    if rc
      (1..7).each do |day|
        counts[:recipient][day-1] += 1 if rc["day:#{day}"]
      end
    end

    if experiment.responses['rp_survey']
      completed += 1
    end
  end

  puts "Total responses: #{total_responses}"
  puts "Completed responses: #{completed}"
  puts "Introduction counts: #{counts[:intro]}"
  puts "Bystander counts: #{counts[:bystander]}"
  puts "Recipient counts: #{counts[:recipient]}"
end

