class AddPlanDataToGeneratedPlans < ActiveRecord::Migration
  def change
    add_column :generated_plans, :plan_data, :text
  end
end
