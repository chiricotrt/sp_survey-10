class AddScreenoutToUsers < ActiveRecord::Migration
  def change
    add_column :users, :screenout, :boolean, default: false
  end
end
