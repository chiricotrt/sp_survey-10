class AddPqResponseSetIdToPqTables < ActiveRecord::Migration
  def change
    add_column :pq_individual_demographics, :pq_response_set_id, :integer
    add_column :pq_households, :pq_response_set_id, :integer
    add_column :pq_mobility_privates, :pq_response_set_id, :integer
    add_column :pq_shared_mobilities, :pq_response_set_id, :integer
    add_column :pq_public_transport_mobilities, :pq_response_set_id, :integer
    add_column :pq_mobility_informations, :pq_response_set_id, :integer
    add_column :pq_mobility_attitudes, :pq_response_set_id, :integer
  end
end
