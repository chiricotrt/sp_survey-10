class ModifyBooleanToIntegerInNationalRailTable < ActiveRecord::Migration
  def change
    remove_column :pre_survey_national_rail_characteristics, :pass_membership
    add_column :pre_survey_national_rail_characteristics, :pass_membership, :integer
  end
end
