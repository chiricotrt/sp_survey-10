class AddParkingSchemeToPreSurveyPrivateMobility < ActiveRecord::Migration
  def change
    add_column :pre_survey_private_mobility_characteristics, :parking_scheme, :integer
  end
end
